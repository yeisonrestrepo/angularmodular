import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from './../components/index';
import { DashboardLayoutComponent } from './dashboardLayout/dashboardLayout.component';

const CONTAINERS = [
  DashboardLayoutComponent
];

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: CONTAINERS,
  exports: CONTAINERS
})
export class LayoutsModule {
}
