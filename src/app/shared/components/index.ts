import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './pageNotFound/pageNotFound.component';

import { PipesModule } from '../pipes';
import { TranslateModule } from 'ng2-translate';

export const COMPONENTS = [
  PageNotFoundComponent
];

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    TranslateModule,
    PipesModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ComponentsModule { }
