import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-integrate-component',
  templateUrl: './integrate.component.html',
  styleUrls: ['./integrate.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IntegrateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
