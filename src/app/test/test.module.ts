import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRoutingModule } from './test-routing.module';
import { IntegrateComponent } from './integrate/integrate.component';
import { DashboardLayoutComponent } from '../shared/layout/dashboardLayout/dashboardLayout.component';

import { TranslateModule } from 'ng2-translate';

@NgModule({
  imports: [
    CommonModule,
    TestRoutingModule,
    TranslateModule
  ],
  declarations: [
    DashboardLayoutComponent,
    IntegrateComponent
  ],
  exports: [
    TestRoutingModule
  ]
})
export class TestModule { }
