import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntegrateComponent } from './integrate/integrate.component';

const routes: Routes = [
  { path: 'prueba', component: IntegrateComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRoutingModule { }
