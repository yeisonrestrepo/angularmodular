import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TestModule } from './test/test.module';
import { AppComponent } from './app.component';

import {
  HttpModule,
  RequestOptions,
  XHRBackend,
  Http
} from '@angular/http';

import { Router } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';

import { ComponentsModule } from './shared/components';
import { LayoutsModule } from './shared/layout';

import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';

// Store
import { store } from './shared/store';

// Third party libraries
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { HttpServiceModule } from './shared/asyncServices/http/http.module';
import { UtilityModule } from './shared/utility';

import { TranslateService } from 'ng2-translate';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';

// Services
import { ConfigService } from './app-config.service';

export function configServiceFactory(config: ConfigService) {
  return () => config.load()
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    ComponentsModule,

    TranslateModule.forRoot(),

    // App custom dependencies
    HttpServiceModule.forRoot(),
    UtilityModule.forRoot(),

    TestModule,
    AppRoutingModule,

    StoreModule.forRoot(store),

    StoreDevtoolsModule.instrument()
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [ConfigService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(router: Router) {
    console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  }
}
